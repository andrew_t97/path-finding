import a_star
import dijkstra
import random
import argparse

def generate_maze(width, length):
    """Generates a random maze containing walls in random positions

    Args:
        width (int): The width of the maze
        length (int): The length/height of the maze

    Returns:
        list: Returns a list of lists of integers which contain 1s and 0s, 1s
        indicate the presence of a wall.
    """
    maze = []
    BASE_WALL_PROB = 0.7
    WALL_VALUE = 1

    for x in range(length):
        wall_count = 0
        current_row = []
        for y in range(width):
            if random.uniform(0, 1) > BASE_WALL_PROB:
                wall_count += 1
                current_row.append(WALL_VALUE)
            else:
                current_row.append(0)
        maze.append(current_row)
    
    return maze

if __name__ == "__main__":
    result = -4
    WIDTH = 50
    HEIGHT = 50

     # Creating parser to allow user to select different execution options
    parser = argparse.ArgumentParser(description="This script allows each path finding algorithm to be run and visualised")
    parser.add_argument("-a", "--astar", action="store_true", dest="astar",
                        help="Runs the A* path finding algorithm")
    parser.add_argument("-d", "--dijkstras", action="store_true", help="Runs the Dijkstra's path finding algorithm")
    parser.add_argument("-b", "--both", action="store_true", help="Runs both A* and Dijkstra's algorithm on the same map")
    
    args = parser.parse_args()
    astar_result = -2
    dijkstra_result = -2
    run_astar = args.astar or args.both
    run_dijkstra = args.dijkstras or args.both
    
    if run_astar or run_dijkstra:
        print("Generating new maze...")
        maze = generate_maze(WIDTH, HEIGHT)
        print("Done!")

    while(run_astar and astar_result < 0):
        if astar_result == -1:
            print("Generating new maze...")
            maze = generate_maze(WIDTH, HEIGHT)
            print("Done!")
        
        astar_result = a_star.run_astar(maze)            

    while(run_dijkstra and dijkstra_result < 0):
        if dijkstra_result == -1:
            print("Generating new maze...")
            maze = generate_maze(WIDTH, HEIGHT)
            print("Done!")

        dijkstra_result = dijkstra.run_dijkstras(maze)

        
    